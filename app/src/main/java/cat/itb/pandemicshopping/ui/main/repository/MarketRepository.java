package cat.itb.pandemicshopping.ui.main.repository;

import android.app.Application;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.model.LatLng;


import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.PhotoMetadata;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPhotoRequest;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.util.Arrays;
import java.util.List;

import cat.itb.pandemicshopping.R;
import cat.itb.pandemicshopping.ui.main.model.nearby.NearbySearchResponse;
import cat.itb.pandemicshopping.ui.main.model.nearby.Supermarket;
import cat.itb.pandemicshopping.ui.main.model.place_details.PlaceDetailsResponse;
import cat.itb.pandemicshopping.ui.main.service.MarketService;
import cat.itb.pandemicshopping.ui.main.screens.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MarketRepository {
    MainActivity mainActivity;
    private MarketService service;
    private Application application;

    private final String BASE_URL = "https://maps.googleapis.com/maps/api/place/";
    private String API_KEY;
    private final String REQUEST_LANGUAGE_CODE = "es";

    private PlacesClient placesClient;

    private MutableLiveData<List<Supermarket>> nearbyMarkets = new MutableLiveData<>();
    private MutableLiveData<List<String>> marketSchedule = new MutableLiveData<>();
    private MutableLiveData<Place> placeDetails = new MutableLiveData<>();
    private MutableLiveData<Bitmap> placePhoto =  new MutableLiveData<>();


    public MarketRepository(Application application) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(MarketService.class);
        this.application = application;
        API_KEY = this.application.getString(R.string.google_maps_key);

        // Initialize the SDK
        Places.initialize(application.getApplicationContext(), API_KEY);

        // Create a new Places client instance
        placesClient = Places.createClient(application.getApplicationContext());
    }

    public LiveData<List<Supermarket>> getNearbySupermarkets(LatLng location, int radius) {
        Call<NearbySearchResponse> call = service.getNearbySupermarkets(API_KEY, location.latitude + "," + location.longitude, radius , Place.Type.GROCERY_OR_SUPERMARKET.toString().toLowerCase(), REQUEST_LANGUAGE_CODE);
        call.enqueue(new Callback<NearbySearchResponse>() {
            @Override
            public void onResponse(Call<NearbySearchResponse> call, Response<NearbySearchResponse> response) {
                NearbySearchResponse nearbySearchResponse = response.body();
                if (!nearbySearchResponse.getStatus().equals("OK")) {
                    Log.i("GET_NEARBY_PLACES", nearbySearchResponse.getStatus());
                    return;
                }
                MarketRepository.this.nearbyMarkets.setValue(nearbySearchResponse.getSupermarkets());
                for (Supermarket place : nearbyMarkets.getValue()) {

                    Log.i("GET_NEARBY_PLACES", place.toString());
                }
            }

            @Override
            public void onFailure(Call<NearbySearchResponse> call, Throwable t) {
                Log.i("GET_NEARBY_PLACES", t.getMessage());
            }
        });
        return nearbyMarkets;
    }

    public LiveData<List<String>> getSchedules(String id) {
        Call<PlaceDetailsResponse> call = service.getSchedules(API_KEY, id, "opening_hours/weekday_text");
        call.enqueue(new Callback<PlaceDetailsResponse>() {
            @Override
            public void onResponse(Call<PlaceDetailsResponse> call, Response<PlaceDetailsResponse> response) {
                PlaceDetailsResponse placeDetailsResponse = response.body();
                if (!placeDetailsResponse.getStatus().equals("OK")) {
                    Log.i("GET_NEARBY_PLACES", placeDetailsResponse.getStatus());
                    return;
                }
                MarketRepository.this.marketSchedule.setValue(placeDetailsResponse.getResult().getOpeningHours().getWeekdayText());
                for (String day : marketSchedule.getValue()) {
                    Log.i("GET_NEARBY_PLACES", day);
                }
            }

            @Override
            public void onFailure(Call<PlaceDetailsResponse> call, Throwable t) {
                Log.i("GET_NEARBY_PLACES", t.getMessage());
            }
        });
        return marketSchedule;
    }

    public LiveData<Place> getPlaceDetails(String placeId) {

        // Specify the fields to return.
        List<Place.Field> placeFields = Arrays.asList(Place.Field.NAME, Place.Field.OPENING_HOURS, Place.Field.PHOTO_METADATAS);

        // Construct a request object, passing the place ID and fields array.
        FetchPlaceRequest request = FetchPlaceRequest.newInstance(placeId, placeFields);

        placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
            MarketRepository.this.placeDetails.setValue(response.getPlace());
            Log.i("GET_DETAILS", "Place found: " + response.getPlace().getName());
        }).addOnFailureListener((exception) -> {
            if (exception instanceof ApiException) {
                ApiException apiException = (ApiException) exception;
                int statusCode = apiException.getStatusCode();
                // Handle error with given status code.
                Log.e("GET_DETAILS", "Place not found: " + exception.getMessage());
            }
        });
        return placeDetails;
    }

    public LiveData<Bitmap> getPlacePhoto(PhotoMetadata photoMetadata){

            // Get the attribution text.
            String attributions = photoMetadata.getAttributions();


            // Create a FetchPhotoRequest.
            FetchPhotoRequest photoRequest = FetchPhotoRequest.builder(photoMetadata)
                    .setMaxWidth(500) // Optional.
                    .setMaxHeight(300) // Optional.
                    .build();
            placesClient.fetchPhoto(photoRequest).addOnSuccessListener((fetchPhotoResponse) -> {
                MarketRepository.this.placePhoto.setValue(fetchPhotoResponse.getBitmap());
            }).addOnFailureListener((exception) -> {
                if (exception instanceof ApiException) {
                    ApiException apiException = (ApiException) exception;
                    int statusCode = apiException.getStatusCode();
                    // Handle error with given status code.
                    Log.e("GET_PHOTO", "Place not found: " + exception.getMessage());
                }
            });
            return placePhoto;
    }
}
