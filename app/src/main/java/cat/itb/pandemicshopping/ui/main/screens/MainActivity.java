package cat.itb.pandemicshopping.ui.main.screens;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.cardview.widget.CardView;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.app.Dialog;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.xw.repo.BubbleSeekBar;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.material.button.MaterialButton;

import java.text.Normalizer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cat.itb.pandemicshopping.R;
import cat.itb.pandemicshopping.ui.main.MainViewModel;
import cat.itb.pandemicshopping.ui.main.model.nearby.Geometry;
import cat.itb.pandemicshopping.ui.main.model.nearby.Supermarket;
import cat.itb.pandemicshopping.ui.main.permissions.PermissionUtils;
import cat.itb.pandemicshopping.ui.main.views.StartScreen;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MainActivity extends AppCompatActivity implements
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback,
        View.OnClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleMap.OnMapLongClickListener{

    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    private final String TAG = "GET_NEARBY_SUPERMARKETS";

    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */
    private boolean mPermissionDenied = false;

    private Location mDeviceLocation;

    private MainViewModel mViewModel;

    private Marker userMarker;
    FirebaseUser firebaseUser;

    private GoogleMap mMap;
    private MaterialButton nearbyMarketsButton;
    private MaterialButton nearbyMarketsButtonLocationSelected;

    private LiveData<List<Supermarket>> nearbyMarkets;

    private LiveData<List<String>> schedule;

    private Map<Supermarket, Marker> supermarketMarkerMap = new HashMap<>();

    private CardView cardView;
    private TextView textDistance;
    private BubbleSeekBar seekBar;
    public int filtreNou = 200;
    private MaterialButton btnAplicar;
    private Dialog dialogCompartir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        String apiKey = getString(R.string.google_maps_key);

        // Setup Places Client
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }

        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        //nearbyMarkets = new MutableLiveData<>();


        nearbyMarketsButton = findViewById(R.id.nearbyMarketsButton);
        nearbyMarketsButton.setOnClickListener(this);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        cardView = findViewById( R.id.filterBar );
        textDistance = findViewById( R.id.textDistance );
        cardView.setOnClickListener( this::clickCard );

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void clickCard(View view) {
        dialogCompartir = new Dialog(view.getContext());
        dialogCompartir.setContentView(R.layout.dialog_slider );
        dialogCompartir.show();

        btnAplicar = dialogCompartir.findViewById( R.id.btnAplicar );
        seekBar = dialogCompartir.findViewById( R.id.seekBar2 );
        btnAplicar.setOnClickListener(this::actualizarFiltro );


    }
    public int actualizarFiltro(View view) {
        filtreNou = seekBar.getProgress();
        String filtreNouText = filtreNou+"";
        dialogCompartir.dismiss();
        textDistance.setText( filtreNouText+"m" );
        return filtreNou;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.setOnMapLongClickListener(this);

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.styled_map));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        miUbicacion();
        enableMyLocation();

    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        // [START maps_check_location_permission]
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            // Permission to access the location is missing. Show rationale and request permission
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    ACCESS_FINE_LOCATION, true);
        }
        // [END maps_check_location_permission]
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    // [START maps_check_location_permission_result]
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults, ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Permission was denied. Display an error message
            // [START_EXCLUDE]
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
            // [END_EXCLUDE]
        }
    }
    // [END maps_check_location_permission_result]

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                mDeviceLocation = location;
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {
            mDeviceLocation.set(null);

        }
    };

    public void miUbicacion() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            mDeviceLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 15000, 0, locationListener);
        }

    }

    private void onSupermarketsChange(List<Supermarket> supermarkets) {

        clearMarkets();
        for(Supermarket supermarket : supermarkets){

            Geometry geometry = supermarket.getGeometry();
            LatLng latLng = new LatLng(geometry.getLocation().getLat(), geometry.getLocation().getLng());
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(
                            getMarketIconBitmap(getDrawableIcon(supermarket.getName()))))
                    .snippet("+ info")
                    .title(supermarket.getName()));
            supermarketMarkerMap.put(supermarket, marker);
    }
    }

    public void mapMarketLocations(LatLng latLng){
        nearbyMarkets = mViewModel.getNearbySupermarkets(latLng, filtreNou);
        nearbyMarkets.observe(this, this::onSupermarketsChange);
    }

    public void clearMarkets(){
        for (Map.Entry<Supermarket,Marker> entry : supermarketMarkerMap.entrySet()){
            entry.getValue().remove();
        }
        supermarketMarkerMap.clear();
    }

    private int getDrawableIcon(String supermarketName){

        String normalized = Normalizer.normalize(supermarketName, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "")
                .toLowerCase();

        String[] arrayMarkets = getResources().getStringArray(R.array.arrayMarkets);

        for (String marketName : arrayMarkets){

            int imageId = getResources().getIdentifier("logo_" + marketName, "drawable", getPackageName());
            if (normalized.contains(marketName)) return imageId;
        }
        return R.drawable.logo_default;
    }

    private Bitmap getMarketIconBitmap(@DrawableRes int drawableRes) {
        Bitmap result = null;
        try {
            result = Bitmap.createBitmap(dp(62), dp(76), Bitmap.Config.ARGB_8888);
            result.eraseColor(Color.TRANSPARENT);
            Canvas canvas = new Canvas(result);
            Drawable drawable = getDrawable(R.drawable.livepin);
            drawable.setBounds(0, 0, dp(62), dp(76));
            drawable.draw(canvas);

            Paint roundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            RectF bitmapRect = new RectF();
            canvas.save();

            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), drawableRes);
            //Bitmap bitmap = BitmapFactory.decodeFile(path.toString()); /*generate bitmap here if your image comes from any url*/
            if (bitmap != null) {
                BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                Matrix matrix = new Matrix();
                float scale = dp(52) / (float) bitmap.getWidth();
                matrix.postTranslate(dp(5), dp(5));
                matrix.postScale(scale, scale);
                roundPaint.setShader(shader);
                shader.setLocalMatrix(matrix);
                bitmapRect.set(dp(5), dp(5), dp(52 + 5), dp(52 + 5));
                canvas.drawRoundRect(bitmapRect, dp(26), dp(26), roundPaint);
            }
            canvas.restore();
            try {
                canvas.setBitmap(null);
            } catch (Exception e) {}
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return result;
    }

    public int dp(float value) {
        if (value == 0) {
            return 0;
        }
        return (int) Math.ceil(getResources().getDisplayMetrics().density * value);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.nearbyMarketsButton:
                LatLng latLngDevice = new LatLng(mDeviceLocation.getLatitude(), mDeviceLocation.getLongitude());
                mapMarketLocations(latLngDevice);
                break;
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        for (Map.Entry<Supermarket, Marker> entry : supermarketMarkerMap.entrySet()){
            if (marker.equals(entry.getValue())){

                Intent intent = new Intent(MainActivity.this, SupermarketDetailActivity.class);
                intent.putExtra("market", entry.getKey());
                startActivity(intent);
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if (supermarketMarkerMap.containsValue(marker)){
            marker.showInfoWindow();
        }
        return false;
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        if(userMarker != null)
            userMarker.remove();
        mapMarketLocations(latLng);
        userMarker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker())
                .title("My Marker"));
    }

    @Override
    public void onStart() {
        super.onStart();
        if (firebaseUser == null) {
            Intent myIntent = new Intent(MainActivity.this, StartScreen.class);
            MainActivity.this.startActivity(myIntent);
        }else{
            System.out.println("INFO DE LA CUENTA: \nID: "+ firebaseUser.getUid()+" User: "+ firebaseUser.getDisplayName()+" Email: "+ firebaseUser.getEmail());

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            Map<String, Object> user = new HashMap<>();
            user.put("name", firebaseUser.getDisplayName());
            user.put("email", firebaseUser.getEmail());

            db.collection("user")
                    .add(user)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error adding document", e);
                        }
                    });
        }

    }
}



