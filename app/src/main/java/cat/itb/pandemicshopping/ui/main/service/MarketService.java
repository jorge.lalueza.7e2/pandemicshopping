package cat.itb.pandemicshopping.ui.main.service;


import cat.itb.pandemicshopping.ui.main.model.nearby.NearbySearchResponse;
import cat.itb.pandemicshopping.ui.main.model.place_details.PlaceDetailsResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface MarketService {

    @GET("nearbysearch/json")
    Call<NearbySearchResponse> getNearbySupermarkets(@Query("key") String key, @Query("location") String location, @Query("radius") double radius, @Query("type") String type, @Query("language") String languageCode);

    @GET("details/json")
    Call<PlaceDetailsResponse> getSchedules(@Query("key") String key, @Query("place_id") String placeId, @Query("fields") String fields);
}
