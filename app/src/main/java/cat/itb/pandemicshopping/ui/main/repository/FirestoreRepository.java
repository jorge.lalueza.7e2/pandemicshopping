package cat.itb.pandemicshopping.ui.main.repository;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FirestoreRepository {

    private Application application;

    private static final String TAG = "FIRESTORE_REPOSITORY:";
    private FirebaseFirestore firestore;
    private MutableLiveData<DocumentSnapshot> documentSnapshot = new MutableLiveData<>();

    private MutableLiveData<List<DocumentSnapshot>> docs = new MutableLiveData<>();

    public FirestoreRepository(Application application) {
        this.application = application;
        FirebaseApp.initializeApp(application.getApplicationContext());
        this.firestore = FirebaseFirestore.getInstance();
    }

    public LiveData<DocumentSnapshot> getDocument(String collectionPath, String documentID) {
        DocumentReference docRef = firestore.collection(collectionPath).document(documentID);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        FirestoreRepository.this.documentSnapshot.setValue(document);
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
        return documentSnapshot;
    }
//
//    public void getDocumentByField(String collectionPath, String field, Object value) {
//        List<DocumentSnapshot> documents = new ArrayList<>();
//        firestore.collection(collectionPath)
//                .whereEqualTo(field, value)
//                .get()
//                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                        if (task.isSuccessful() && task.getResult() != null) {
//                            for (QueryDocumentSnapshot document : task.getResult()) {
//                                documents.add(document);
//                                Log.d(TAG, document.getId() + " => " + document.getData());
//                                if (documents.size() == 1){
//                                    callback.onSnapshot(documents.get(0));
//                                }
//                            }
//                        } else {
//                            Log.d(TAG, "Error getting documents: ", task.getException());
//                        }
//                    }
//                });
//    }
//
    public MutableLiveData<List<DocumentSnapshot>> getAllDocs(String collectionPath) {
        List<DocumentSnapshot> documents = new ArrayList<>();
        firestore.collection(collectionPath)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                documents.add(document);
                                //Log.d(TAG, document.getId() + " => " + document.getData());
                            }
                            FirestoreRepository.this.docs.setValue(documents);
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        return docs;
    }

    public void addDocumentWithRandomID(String collectionPath, Map<String,Object> docRef) {
        firestore.collection(collectionPath)
                .add(docRef)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });
        // [END add_document]
    }

    public void addDocumentWithCustomID(String collectionPath, Map<String,Object> docRef, String documentID) {
        firestore.collection(collectionPath).document(documentID)
                .set(docRef)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written with ID: " + documentID);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });
        // [END add_document]
    }

    public void updateDocument(String collectionPath, String documentId, String field, Object value) {
        // [START update_document]
        DocumentReference docRef = firestore.collection(collectionPath).document(documentId);

        docRef
                .update(field, value)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully updated!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error updating document", e);
                    }
                });
        // [END update_document]
    }

    public void deleteDocument(String collectionPath, String documentID){
        firestore.collection(collectionPath).document(documentID)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error deleting document", e);
                    }
                });
    }
}
