package cat.itb.pandemicshopping.ui.main.screens;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.google.android.libraries.places.api.model.PhotoMetadata;
import com.google.android.libraries.places.api.model.Place;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cat.itb.pandemicshopping.R;
import cat.itb.pandemicshopping.ui.main.MainViewModel;
import cat.itb.pandemicshopping.ui.main.model.nearby.Supermarket;

public class SupermarketDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private MainViewModel mViewModel;
    private LiveData<List<String>> schedule;
    private LiveData<Place> placeLiveData;
    private LiveData<Bitmap> placePhoto;

    private  DocumentSnapshot placeDoc;

    TextView textViewTime;

    FirebaseUser firebaseUser;

    TextView supermarketName;
    ImageView supermarketImage;
    TextView supermarketAddress;
    TextView supermarketSchedule;

    Supermarket supermarket;

    private final String COLLECTION_PATH = "supermarkets";

    String times[] = {"5 min", "10 min", "+ 10 min"};
    String record = "";
    ArrayAdapter<String> adapter;


    Spinner waitingTimesSpinner;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.supermarket_detail);

        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        supermarket = (Supermarket) getIntent().getSerializableExtra("market");

        supermarketName = findViewById(R.id.supermarketName);
        supermarketImage = findViewById(R.id.supermarketImage);
        supermarketAddress = findViewById(R.id.supermarketAddress);
        supermarketSchedule = findViewById(R.id.supermarketSchedule);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        supermarketName.setText(supermarket.getName());
        supermarketAddress.setText(supermarket.getVicinity());

        findViewById(R.id.closeBack).setOnClickListener(this);

        placeLiveData = mViewModel.getPlaceDetails(supermarket.getPlaceId());
        placeLiveData.observe(this, this::observeDetails);

        findViewById(R.id.addTimeQueueBtn).setOnClickListener(this);
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, times);
        textViewTime = findViewById(R.id.textViewTime);

        placeDoc = mViewModel.getMarketDocument(COLLECTION_PATH, supermarket.getPlaceId()).getValue();
        if (placeDoc == null) {
            mViewModel.addDocument(COLLECTION_PATH, getMapMarket(supermarket), supermarket.getPlaceId());
        } else {
            List<DocumentSnapshot> docs = mViewModel.getAllDocs(COLLECTION_PATH + "/" + supermarket.getPlaceId() + "/waitingQueues").getValue();

            if(docs != null) {
                DocumentSnapshot lastUpdate = docs.get(docs.size() - 1);
                if(lastUpdate.getString("waitingTime") != null)
                    textViewTime.setText(lastUpdate.getString("waitingTime"));
            }

        }

        waitingTimesSpinner = findViewById(R.id.spinTime);

        waitingTimesSpinner.setAdapter(adapter);

        waitingTimesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position)
                {
                    case 0:
                        record = "5 min";
                        break;

                    case 1:
                        record = "10 min";
                        break;

                    case 2:
                        record = "+10 min";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void observeDetails(Place place) {
        if (place.getOpeningHours() != null){
            List<String> days = place.getOpeningHours().getWeekdayText();
            String fullSchedule = "";
            for (String day : days){
                if (day.equals(days.get(days.size()-1))){
                    fullSchedule = fullSchedule.concat(day);
                }else {
                    fullSchedule = fullSchedule.concat(day + "\n");
                }
            }
            supermarketSchedule.setText(fullSchedule);
        }

        if (place.getPhotoMetadatas() != null){
            PhotoMetadata photoMetadata = place.getPhotoMetadatas().get(0);
            placePhoto = mViewModel.getPlacePhoto(photoMetadata);
            placePhoto.observe(this, this::observePhoto);
        }
    }

    private void observePhoto(Bitmap bitmap) {
        Glide.with(supermarketImage.getContext())
                .load(bitmap)
                .into(supermarketImage);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.closeBack:
                onBackPressed();
                break;
            case R.id.addTimeQueueBtn:
                addQueueTime(v);
        }
    }

    public void addQueueTime(View view) {
        Map<String,Object> queue = new HashMap<>();
        queue.put("date", new Date());
        queue.put("displayName", firebaseUser.getDisplayName());
        queue.put("userId", firebaseUser.getUid());
        queue.put("waitingTime", record);
        mViewModel.addDocument(COLLECTION_PATH + "/" + supermarket.getPlaceId() + "/waitingQueues", queue);
        textViewTime.setTextSize(18);
        textViewTime.setText(record);
    }

    public Map<String, Object> getMapMarket(Supermarket supermarket) {
        Map<String, Object> documentMarket = new HashMap<>();
        documentMarket.put("name", supermarket.getName());
        return documentMarket;
    }
}
