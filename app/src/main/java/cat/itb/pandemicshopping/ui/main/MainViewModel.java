package cat.itb.pandemicshopping.ui.main;
import android.app.Application;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.PhotoMetadata;
import com.google.android.libraries.places.api.model.Place;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.List;
import java.util.Map;

import cat.itb.pandemicshopping.ui.main.repository.FirestoreRepository;
import cat.itb.pandemicshopping.ui.main.repository.MarketRepository;

import cat.itb.pandemicshopping.ui.main.model.nearby.Supermarket;

public class MainViewModel extends AndroidViewModel {

    private MarketRepository marketRepository;

    private FirestoreRepository firestoreRepository;


    public MainViewModel(@NonNull Application application) {
        super(application);
        firestoreRepository = new FirestoreRepository(application);
        marketRepository = new MarketRepository(application);
    }

    public LiveData<List<Supermarket>> getNearbySupermarkets(LatLng location, int radius){
        return marketRepository.getNearbySupermarkets(location,radius);
    }

    public LiveData<List<DocumentSnapshot>> getAllDocs(String collectionPath){
        return firestoreRepository.getAllDocs(collectionPath);
    }

    public LiveData<List<String>> getSchedules(String id){
        return marketRepository.getSchedules(id);
    }

    public LiveData<Place> getPlaceDetails(String placeId){
        return marketRepository.getPlaceDetails(placeId);
    }

    public LiveData<Bitmap> getPlacePhoto(PhotoMetadata photoMetadata){
        return marketRepository.getPlacePhoto(photoMetadata);
    }

    public LiveData<DocumentSnapshot> getMarketDocument(String collectionPath, String documentID){
        return  firestoreRepository.getDocument(collectionPath, documentID);
    }

    public void addDocument(String collectionPath, Map<String,Object> docRef, String documentID){
        firestoreRepository.addDocumentWithCustomID(collectionPath, docRef, documentID);
    }

    public void addDocument(String collectionPath, Map<String, Object> docRef){
        firestoreRepository.addDocumentWithRandomID(collectionPath, docRef);
    }

    public void updateDocument(String collecitonPath, String documentId, String field, Object value){
        firestoreRepository.updateDocument(collecitonPath, documentId, field, value);
    }

}
